import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

/**
 *
 * Beschreibung
 *
 * @version 1.0 vom 21.05.2019
 * @author 
 */

public class tt extends JFrame {
  // Anfang Attribute
  private JTabbedPane jTabbedPane1 = new JTabbedPane();
    private JPanel jTabbedPane1TabPanelChat = new JPanel(null, true);
      private JTextArea jTextArea1 = new JTextArea("");
        private JScrollPane jTextArea1ScrollPane = new JScrollPane(jTextArea1);
      private JTextField jTextField1 = new JTextField();
      private JButton bSenden = new JButton();
      private JComboBox<String> jComboBox1 = new JComboBox<String>();
        private DefaultComboBoxModel<String> jComboBox1Model = new DefaultComboBoxModel<String>();
      private JLabel lUsername = new JLabel();
      private JLabel lHost = new JLabel();
      private JLabel lPort = new JLabel();
      private JTextField jTextField2 = new JTextField();
      private JTextField jTextField3 = new JTextField();
      private JTextField jTextField4 = new JTextField();
    private JPanel jTabbedPane1TabPanelSettings = new JPanel(null, true);
  // Ende Attribute
  
  public tt() { 
    // Frame-Initialisierung
    super();
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    int frameWidth = 597; 
    int frameHeight = 325;
    setSize(frameWidth, frameHeight);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width - getSize().width) / 2;
    int y = (d.height - getSize().height) / 2;
    setLocation(x, y);
    setTitle("tt");
    setResizable(false);
    Container cp = getContentPane();
    cp.setLayout(null);
    // Anfang Komponenten
    
    jTabbedPane1.setBounds(2, 5, 576, 278);
    jTabbedPane1.addTab("Chat", jTabbedPane1TabPanelChat);
    jTabbedPane1.addTab("Settings", jTabbedPane1TabPanelSettings);
  
    
    jTabbedPane1.setFont(new Font("Arial", Font.PLAIN, 16));
    jTabbedPane1.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
    jTabbedPane1.setTabPlacement(JTabbedPane.TOP);
    jTabbedPane1.setForeground(Color.BLACK);
    jTabbedPane1.setBackground(Color.WHITE);
    cp.add(jTabbedPane1);
    //jTabbedPane1TabPanelChat.setParentCustomHint(false);

    jTabbedPane1TabPanelChat.setBackground(Color.WHITE);
    jTabbedPane1TabPanelChat.setOpaque(true);
    jTextArea1ScrollPane.setBounds(2, 22, 568, 212);
    jTextArea1.setEditable(false);
    jTabbedPane1TabPanelChat.add(jTextArea1ScrollPane);
    jTextField1.setBounds(140, 219, 318, 28);
    jTextField1.setFont(new Font("Arial", Font.PLAIN, 16));
    jTabbedPane1TabPanelChat.add(jTextField1);
    bSenden.setBounds(462, 220, 107, 25);
    bSenden.setText("Senden");
    bSenden.setMargin(new Insets(2, 2, 2, 2));
    bSenden.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        bSenden_ActionPerformed(evt);
      }
    });
    bSenden.setFont(new Font("Arial", Font.PLAIN, 16));
    jTabbedPane1TabPanelChat.add(bSenden);
    jComboBox1.setModel(jComboBox1Model);
    jComboBox1.setBounds(2, 219, 134, 28);
    jComboBox1.setFont(new Font("Arial", Font.PLAIN, 16));
    jComboBox1Model.addElement("Alle");
    jTabbedPane1TabPanelChat.add(jComboBox1);
    lUsername.setBounds(12, 16, 110, 20);
    lUsername.setText("Username:");
    lUsername.setEnabled(true);
    lUsername.setBackground(Color.WHITE);
    lUsername.setOpaque(true);
    jTabbedPane1TabPanelChat.add(lUsername);
    lHost.setBounds(12, 48, 110, 20);
    lHost.setText("Host:");
    lHost.setEnabled(true);
    lHost.setBackground(Color.WHITE);
    lHost.setOpaque(true);
    jTabbedPane1TabPanelChat.add(lHost);
    lPort.setBounds(12, 80, 110, 22);
    lPort.setText("Port:");
    lPort.setFont(new Font("Arial", Font.PLAIN, 16));
    lPort.setBackground(Color.WHITE);
    lPort.setOpaque(true);
    jTabbedPane1TabPanelChat.add(lPort);
    jTextField2.setBounds(140, 32, 150, 20);
    jTextField2.setFont(new Font("Arial", Font.PLAIN, 16));
    jTabbedPane1TabPanelChat.add(jTextField2);
    jTextField3.setBounds(140, 64, 150, 20);
    jTextField3.setFont(new Font("Arial", Font.PLAIN, 16));
    jTabbedPane1TabPanelChat.add(jTextField3);
    jTextField4.setBounds(140, 96, 150, 20);
    jTextField4.setFont(new Font("Arial", Font.PLAIN, 16));
    jTabbedPane1TabPanelChat.add(jTextField4);
    // Ende Komponenten
    
    setVisible(true);
  } // end of public tt
  
  // Anfang Methoden
  
  public static void main(String[] args) {
    new tt();
  } // end of main
  
  public void bSenden_ActionPerformed(ActionEvent evt) {
    // TODO hier Quelltext einf�gen
    
  } // end of bSenden_ActionPerformed

  // Ende Methoden
} // end of class tt

