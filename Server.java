/**
 *
 * Beschreibung
 *
 * @version 1.0 vom 05.05.2019
 * @author 
 */
import java.util.*;
import socketio.*;

public class Server {
  
  int max;
  Socket client;
  ServerSocket server;
  String[] nicks;
  Server_Handler[] handler;
  Scanner scan = new Scanner(System.in);
  
  public Server() {
    try {
      server = new ServerSocket(1234);
      System.out.println("Konsole: Server erstellt");
      
      System.out.print("Anzahl der maximalen Clients: ");
      max = scan.nextInt();
      nicks = new String[max];
      handler = new Server_Handler[max];
      
      for (int i = 0; i < max ;i++ ) {
        client = server.accept();
        System.out.println("Konsole: Client wurde akzeptiert");
        while (client.dataAvailable() <= 0);
        nicks[i] = client.readLine();
        handler[i] = new Server_Handler(client, this);
        Thread th = new Thread(handler[i]);
        th.start();
        sendToAll("Konsole: " + nicks[i] + " ist dem Chat beigetreten");
      } // end of for
      
      sendToAll("Konsole: Der Chat-Room ist voll");
    } catch(Exception e) {
      System.out.println("Fehler 1: " + e.getMessage());
    }
  }
  
  public void sendToAll(String temp) {
    try {
      for (int i = 0;i<max ;i++ ) {
        handler[i].client.write(temp + "\n");
      } // end of for
      System.out.println(temp);
    } catch(Exception e) {
      System.out.println("Fehler 2: " + e.getMessage());
    }
  }
  
  public static void main(String[] args) {
    new Server();
  } // end of main

} // end of class Server

