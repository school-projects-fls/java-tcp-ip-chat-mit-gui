/**
 *
 * Beschreibung
 *
 * @version 1.0 vom 05.05.2019
 * @author 
 */

import java.util.*;
import socketio.*;

public class Client {
  
  Socket client;
  Scanner scan = new Scanner(System.in);
  
  public Client() {
    try {
      client = new Socket("127.0.0.1", 1234);
      client.connect();
      
      String nick = scan.nextLine();
      client.write(nick + "\n");
      
      Thread th1 = new Thread(new Sender(client, nick));
      th1.start();
      Thread th2 = new Thread(new Empfanger(client));
      th2.start();
    } catch(Exception e) {
      System.out.println("Fehler 1: " + e.getMessage());
    }
  }
  
  public static void main(String[] args) {
    new Client();
  } // end of main
  
  public class Sender implements Runnable {
    
    Socket client;
    String nick;
    Scanner scan = new Scanner(System.in);
    boolean open = true;
    
    public Sender(Socket client, String nick) {
      this.client = client;
      this.nick = nick;
    }
    
    public void run() {
      while (open) { 
        try {
          String temp = scan.next();
          client.write(nick + ": " + temp + "\n");
        } catch(Exception e) {
          System.out.println("Fehler: " + e.getMessage());
        }
      } // end of while
    }
    
  }
  
  public class Empfanger implements Runnable {
    
    Socket client;
    boolean open = true;
    
    public Empfanger(Socket client) {
      this.client = client;
    }
    
    public void run() {
      while (open) { 
        try {
          while (client.dataAvailable() <= 0);
          System.out.println(client.readLine());
        } catch(Exception e) {
          System.out.println("Fehler: " + e.getMessage());
        } 
      } // end of while
    }
    
  }

} // end of class Client

