/**
 *
 * Beschreibung
 *
 * @version 1.0 vom 05.05.2019
 * @author 
 */
import java.util.*;
import socketio.*;

public class Server_Handler implements Runnable {
  
  Socket client;
  Server serverclass;
  boolean open = true;
  
  public Server_Handler(Socket client, Server serverclass) {
    this.client = client;
    this.serverclass = serverclass;
  }
  
  public void run() {
    while (open) { 
      try {
        while (client.dataAvailable() <= 0);
        String temp = client.readLine();
        serverclass.sendToAll(temp);
      } catch(Exception e) {
        System.out.println("Fehler: " + e.getMessage());
      }
    } // end of while
  }

} // end of class Server_Handler

